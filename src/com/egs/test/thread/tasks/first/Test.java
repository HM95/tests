package com.egs.test.thread.tasks.first;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        ThreadProperties myThread = new ThreadProperties("Employ", 63, 0);
        ThreadProperties myThread1 = new ThreadProperties("Company", 20, 0);
        ThreadProperties myThread2 = new ThreadProperties("People", 50, 0);
        List<ThreadProperties> listOfThreadProperties = new ArrayList<>();
        listOfThreadProperties.add(myThread);
        listOfThreadProperties.add(myThread1);
        listOfThreadProperties.add(myThread2);
        double passedTime = 0.0;
        int check=0;
        for (int i = 0; i < listOfThreadProperties.size(); i++) {
            if (check==listOfThreadProperties.size()){
                break;
            }
            int exTime = listOfThreadProperties.get(i).getExecutionTime();
            if (listOfThreadProperties.get(i).getExecutionTime() == listOfThreadProperties.get(i).getTotalTime()) {
                if (i == listOfThreadProperties.size() - 1) {
                    i = -1;
                }
                check++;
                continue;
            } else if (listOfThreadProperties.get(i).getExecutionTime() + 5 > listOfThreadProperties.get(i).getTotalTime()) {
                listOfThreadProperties.get(i).setExecutionTime(listOfThreadProperties.get(i).getTotalTime());
                int b = listOfThreadProperties.get(i).getTotalTime() - exTime;
                passedTime += b;
            } else {
                exTime += 5;
                listOfThreadProperties.get(i).setExecutionTime(exTime);
                passedTime += 5;
            }
            if (i == listOfThreadProperties.size() - 1) {
                i = -1;
            }
            check=0;
        }
        double midTime = (passedTime / listOfThreadProperties.size());
        System.out.println("All TIME = " + passedTime);
        System.out.println("Mid Time = " + midTime);
    }
}
