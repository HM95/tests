package com.egs.test.thread.tasks.first;

public class ThreadProperties {
    private String threadName;
    private int totalTime;
    private int executionTime;

    public ThreadProperties(String c, int totaltime, int timeexecution) {
        this.threadName = c;
        this.totalTime = totaltime;
        this.executionTime = timeexecution;
    }

    public String getThreadName() {
        return threadName;
    }

    public void setThreadName(String threadName) {
        this.threadName = threadName;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    public int getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(int executionTime) {
        this.executionTime = executionTime;
    }
}
