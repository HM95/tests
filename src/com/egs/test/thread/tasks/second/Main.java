package com.egs.test.thread.tasks.second;

public class Main {
    static int balance = 0;
    static  volatile boolean switchVariable =true;

    public static void main(String[] args) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    while (switchVariable) {
                        balance += 10;
                        System.out.println(balance);
                        switchVariable =false;
                    }
                }
            }
        });
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    while (!switchVariable) {
                        balance -= 10;
                        System.out.println(balance);
                        switchVariable = true;
                    }

                }
            }
        });
        thread.start();
        thread1.start();
    }
}
